// SPDX-License-Identifier: GPL-2.0

//! Soul of Tefexia

#![no_std]

#[macro_use]
extern crate kernel;

use core::ffi::{c_int, c_void};
use core::str::from_utf8;
use core::mem::transmute;

use kernel::bindings::{filp_open, kernel_read, MAX_ERRNO, O_RDONLY, power_supply_get_by_name, power_supply_get_property, power_supply_property_POWER_SUPPLY_PROP_CHARGE_FULL, power_supply_property_POWER_SUPPLY_PROP_CHARGE_NOW, power_supply_property_POWER_SUPPLY_PROP_STATUS, power_supply_propval};
use kernel::file::Operations;
use kernel::io_buffer::IoBufferWriter;
use kernel::io_buffer::IoBufferReader;
use kernel::file::File;
use kernel::miscdev::{Options, Registration};
use kernel::prelude::*;
use kernel::prelude::String;
use kernel::random::getrandom;
use kernel::sync::{Arc, ArcBorrow, Mutex};
use kernel::str::CString;

type SharedMessages = Arc<Pin<Box<Mutex<TefexiaInfo>>>>;

static SLEEPING_FOX_ART: &str = include_str!("fox.txt");

module! {
    type: Tefexia,
    name: "tefexia",
    author: "tmvkrpxl0",
    description: "Cutest fox in the world!",
    license: "GPL",
    params: {
        low_battery: u8 {
            default: 30u8,
            permissions: 0o644,
            description: "Sets threshold for low battery messages",
        },
        messages: str {
            default: b"/usr/share/wisdom",
            permissions: 0o644,
            description: "File path for wisdom/messages!",
        },
    },
}

fn is_err(pointer_address: usize) -> bool {
    pointer_address >= (-(MAX_ERRNO as isize)) as usize
}

impl Tefexia {
    fn load_messages(path: &CStr) -> Option<(Vec<String>, Vec<String>)> {
        pr_info!("{}에서 메세지 불러오는 중...\n", path);
        let file = unsafe {
            filp_open(path.as_char_ptr(), O_RDONLY as c_int, 0o644)
        };
        if file.is_null() {
            pr_warn!("{}에 파일이 존재하지 않습니다!\n", path);
            return None;
        };
        if is_err(file as usize) {
            pr_warn!("파일 구조체 생성 오류!: {}\n", file as usize);
            return None;
        }

        unsafe {
            let mut buffer = [0u8; 8096];
            let reference = (&mut buffer).as_mut_ptr() as *mut c_void;
            let read = kernel_read(file, reference, 8096, &mut 0);
            if read < 0 {
                pr_warn!("파일 {}은(는) 올바른 파일이 아닙니다!\n 오류 코드: {}\n", path, read);
                return None;
            } else if read == 8096 {
                pr_warn!("파일 {}이 너무 큽니다!\n", path);
                return None;
            }

            let str = match from_utf8(&buffer[0..read as usize]) {
                Ok(str) => str,
                Err(e) => {
                    pr_warn!("파일 {}의 내용이 잘못되었습니다! {}\n", path, e);
                    return None;
                }
            };

            enum Subjects {
                None,
                Normal,
                Battery,
            }
            let mut subject = Subjects::None;

            let mut normal: Vec<String> = Vec::new();
            let mut battery: Vec<String> = Vec::new();
            str.lines().for_each(|line| {
                let trimmed = line.trim();
                if trimmed.is_empty() {
                    return;
                }
                match trimmed {
                    "NORMAL" => {
                        subject = Subjects::Normal
                    }
                    "BATTERY" => {
                        subject = Subjects::Battery
                    }
                    _ => {
                        match subject {
                            Subjects::None => {}
                            Subjects::Normal | Subjects::Battery => {
                                let target = if let Subjects::Normal = subject {
                                    &mut normal
                                } else {
                                    &mut battery
                                };

                                let mut vec = Vec::new();
                                if let Err(e) = vec.try_extend_from_slice(trimmed.as_bytes()) {
                                    pr_err!("지혜를 읽어드릴 메모리가 부족합니다!\n{}\n", e);
                                }

                                match String::from_utf8(vec) {
                                    Ok(string) => {
                                        if let Err(e) = target.try_push(string) {
                                            pr_err!("지혜를 읽어드릴 메모리가 부족합니다!\n{}\n", e);
                                        }
                                    }
                                    Err(e) => {
                                        pr_warn!("파일 {}의 내용이 잘못되었습니다! {}\n", path, e);
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            });

            Some((normal, battery))
        }
    }

    fn battery_status() -> Option<(u8, bool)> {
        let name = CStr::from_bytes_with_nul(b"BAT0\0").unwrap();
        unsafe {
            let power = power_supply_get_by_name(name.as_char_ptr());
            if power.is_null() || is_err(power as usize) {
                pr_err!("power_supply 구조체를 파악하는 도중 문제가 발생했습니다!\n");
                return None;
            }

            let mut now = power_supply_propval {
                intval: 0
            };
            let mut full = power_supply_propval {
                intval: 0
            };
            let mut charging = power_supply_propval {
                intval: 0
            };

            let result = power_supply_get_property(
                power,
                power_supply_property_POWER_SUPPLY_PROP_CHARGE_NOW,
                &mut now,
            );
            if result != 0 {
                pr_err!("현재 배터리 정보를 가져오는데 실패했습니다!\n");
                return None;
            }

            let result = power_supply_get_property(
                power,
                power_supply_property_POWER_SUPPLY_PROP_CHARGE_FULL,
                &mut full,
            );
            if result != 0 {
                pr_err!("최대 배터리 용량을 가져오는데 실패했습니다!\n");
                return None;
            };

            let result = power_supply_get_property(
                power,
                power_supply_property_POWER_SUPPLY_PROP_STATUS,
                &mut charging
            );
            if result != 0 {
                pr_err!("현재 배터리 충전 상태를 가져오는데 실패했습니다!\n");
                return None;
            };

            if full.intval == 0 {
                pr_err!("배터리 정보가 잘못되었습니다!");
                return None;
            }

            Some(((now.intval * 100 / full.intval) as u8, charging.intval == 1))
        }
    }
}

impl kernel::Module for Tefexia {
    fn init(name: &'static CStr, module: &'static ThisModule) -> Result<Self> {
        pr_info!("테페시아 두두등장!\n");

        let (message_path, threshold) = {
            let lock = module.kernel_param_lock();
            let threshold = low_battery.read(&lock);
            let data: &[u8] = messages.read(&lock);
            let mut message_path_buffer = Vec::new();

            if let Err(e) = message_path_buffer.try_extend_from_slice(data) {
                pr_err!("파라미터 messages 읽기 위한 메모리가 부족합니다!\n");
                pr_err!("대체 뭘 하시는 거길레 이정도 공간조차 없나요..?\n{}\n", e);
                return Err(ENOMEM);
            }
            let Ok(path) = from_utf8(&message_path_buffer) else {
                pr_err!("전달된 메세지 파일 경로가 잘못되었습니다!\n");
                return Err(EINVAL);
            };
            (CString::try_from_fmt(fmt!("{}", path))?, *threshold)
        };

        let tefexia_info = TefexiaInfo {
            normal: None,
            battery: None,
            low_battery: threshold,
            message_path,
        };

        let boxed = match Box::try_new(unsafe { Mutex::new(tefexia_info) }) {
            Ok(boxed) => boxed,
            Err(e) => {
                pr_err!("메세지 Mutex를 만들 공간조차 없습니다!\n{}\n", e);
                return Err(ENOMEM);
            }
        };
        let mut pin = Box::into_pin(boxed);
        mutex_init!(pin.as_mut(), "TEFEXIA_MESSAGES");

        let shared: SharedMessages = Arc::try_new(pin)?;
        let device = Options::new()
            .mode(644)
            .register_new(
                fmt!("{}", name),
                shared,
            )?;

        let tefexia = Self {
            _device: device,
        };
        Ok(tefexia)
    }
}

impl Drop for Tefexia {
    fn drop(&mut self) {
        pr_info!("다음에 봐요\n")
    }
}

#[vtable]
impl Operations for Tefexia {
    type OpenData = SharedMessages;
    type Data = SharedMessages;

    fn open(context: &Self::OpenData, _: &File) -> Result<Self::Data> {
        Ok(context.clone())
    }

    fn read(
        data: ArcBorrow<'_, Pin<Box<Mutex<TefexiaInfo>>>>,
        _: &File,
        writer: &mut impl IoBufferWriter,
        offset: u64,
    ) -> Result<usize> {
        if writer.is_empty() || offset != 0 {
            return Ok(0);
        }
        let reply_image = writer.len() >= SLEEPING_FOX_ART.as_bytes().len();

        if reply_image {
            let bytes = SLEEPING_FOX_ART.as_bytes();
            writer.write_slice(bytes)?;
            Ok(bytes.len())
        } else {
            let status = Self::battery_status();
            let mut lock = data.lock();
            let is_low = status.map(|(value, charging)| !charging && value <= lock.low_battery).unwrap_or(false);

            let mut random = 0usize;
            unsafe {
                let reference: &mut [u8; 4] = transmute(&mut random);
                getrandom(reference)?;
            }

            if lock.normal.is_none() || lock.battery.is_none() {
                let path = unsafe {
                    CStr::from_char_ptr(lock.message_path.as_char_ptr())
                };
                let Some((normal, battery)) = Self::load_messages(path) else {
                    return Err(EIO);
                };
                lock.normal.replace(normal);
                lock.battery.replace(battery);
            }

            let target = if !is_low {
                lock.normal.as_ref().unwrap()
            } else {
                lock.battery.as_ref().unwrap()
            };
            let index = random % target.len();

            let bytes = target[index].as_bytes();
            writer.write_slice(bytes)?;
            Ok(bytes.len())
        }
    }

    fn write(
        data: ArcBorrow<'_, Pin<Box<Mutex<TefexiaInfo>>>>,
        _: &File,
        reader: &mut impl IoBufferReader,
        offset: u64,
    ) -> Result<usize> {
        if reader.is_empty() || offset != 0 {
            return Ok(0);
        }

        let len = reader.len();
        if len > 1024 {
            return Err(EMSGSIZE);
        }

        let buffer = reader.read_all()?;
        let Ok(string) = String::from_utf8(buffer) else {
            return Err(EINVAL);
        };

        if string.starts_with("motd") {
            let mut split = string.split(' ');
            split.next();
            let Some(path) = split.next() else {
                return Err(EINVAL);
            };

            let Ok(message_path) = CString::try_from_fmt(fmt!("{}", path.trim())) else {
                return Err(EINVAL);
            };

            let mut lock = data.lock();
            lock.message_path = message_path;
            lock.normal = None;
            lock.battery = None;

            Ok(len)
        } else {
            Err(EINVAL)
        }
    }
}

struct TefexiaInfo {
    normal: Option<Vec<String>>,
    battery: Option<Vec<String>>,
    low_battery: u8,
    message_path: CString,
}

struct Tefexia {
    _device: Pin<Box<Registration<Self>>>,
}
