# SPDX-License-Identifier: GPL-2.0

KDIR ?= /lib/modules/`uname -r`/build

all:
	$(MAKE) -C $(KDIR) LLVM=1 M=$$PWD

default:
	$(MAKE) -C $(KDIR) LLVM=1 M=$$PWD

modules_install: default
	$(MAKE) -C $(KDIR) LLVM=1 M=$$PWD modules_install

clean:
	$(MAKE) -C $(KDIR) LLVM=1 M=$$PWD clean
	cargo clean

install:
	insmod ${CURDIR}/tefexia.ko

remove:
	rmmod ${CURDIR}/tefexia.ko

defconfig:
